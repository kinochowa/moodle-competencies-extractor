LOGIN_ID_PAIRS_FILENAME = 'gandalf_id_login_pairs.csv'

CITIES = [
    'All',
    'Bordeaux',
    'Lille',
    'Lyon',
    'Marseille',
    'Montpellier',
    'Nancy',
    'Nantes',
    'Nice',
    'Paris',
    'Rennes',
    'Strasbourg',
    'Toulouse',
]

PROMOTIONS = [
    'all',
    'msc2021',
    'msc2022'
]