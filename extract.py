import requests
from bs4 import BeautifulSoup

import config


KO_FORMAT = 'KO'
OK_FORMAT = 'OK'


def get_view_php(userid: int = None):
    cookies = {
        'MoodleSession': config.SESSION_ID
    }

    if userid:
        response = requests.get(f'{config.BASE_URL}local/graph/view.php?userid={userid}&semester={config.SEMESTER}', cookies=cookies)
    else:
        response = requests.get(f'{config.BASE_URL}local/graph/view.php', cookies=cookies)
    print(response)
    return response.text


def init_worksheet(workbook, sheet_name: str = 'Competencies'):
    worksheet = workbook.add_worksheet(sheet_name[:31])
    title_format = workbook.add_format({'bold': True, 'font_size': 15})

    worksheet.set_column(0, 0, 40)
    worksheet.set_column(1, 1, 80)
    worksheet.set_column(2, 2, 12)
    worksheet.write(0, 0, 'Competencies', title_format)
    worksheet.write(0, 1, 'Behaviors', title_format)
    worksheet.write(0, 2, 'Validated', title_format)
    return worksheet


def __write_behaviors(behaviors: list, worksheet, row: int, cell_format: dict) -> int:

    for behavior in behaviors:
        behavior_str = behavior.find('span', class_='competencyTitle').text.strip()
        if 'proficient' in behavior.get('class'):
            worksheet.write(row, 1, behavior_str, cell_format[OK_FORMAT])
            worksheet.write(row, 2, 'OK', cell_format[OK_FORMAT])
        else:
            worksheet.write(row, 1, behavior_str, cell_format[KO_FORMAT])
            worksheet.write(row, 2, 'OK', cell_format[KO_FORMAT])
        row += 1

    return row


def __write_competencies(competencies: list, workbook, worksheet, row: int) -> int:
    # Cell Formats
    ko_competency_format = workbook.add_format({'bg_color': '#DA9694', 'bold': True, 'font_size': 13})
    ok_competency_format = workbook.add_format({'bg_color': '#C4D79B', 'bold': True, 'font_size': 13})
    ko_behavior_format = workbook.add_format({'bg_color': '#DA9694'})
    ok_behavior_format = workbook.add_format({'bg_color': '#C4D79B'})

    for competency in competencies:
        worksheet.write(row, 0, competency.li.div.find('span', class_='competencyTitle').text.strip(),
                        ok_competency_format if 'proficient' in competency.get('class') else ko_competency_format)
        row += 1

        behaviors = competency.li.find_all('ul', recursive=False)
        row = __write_behaviors(behaviors, worksheet, row,
                                {OK_FORMAT: ok_behavior_format, KO_FORMAT: ko_behavior_format})
    return row


def extract(workbook, user: tuple[int, str] = None):
    content = get_view_php(user[0] if user else None)

    # Add a worksheet and create the header row
    if user:
        worksheet = init_worksheet(workbook, user[1])
    else:
        worksheet = init_worksheet(workbook)

    soup = BeautifulSoup(content, 'html.parser')
    competencies_div = soup.body.find('div', attrs={'id': 'skill-on-demand'}).li

    row = 1
    categories = competencies_div.find_all('ul', recursive=False)
    for category in categories:
        competencies = category.li.find_all('ul', recursive=False)
        row = __write_competencies(competencies, workbook, worksheet, row)

