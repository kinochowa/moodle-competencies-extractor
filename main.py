import csv
import sys
import xlsxwriter

import config
import do_not_touch_config as conf
from extract import extract


def display_help():
    print(f'.{sys.argv[0]} promotion city\n')
    print(f'\t- promotion is one of the following choices: {", ".join(conf.PROMOTIONS)}')
    print(f'\t- city is one of the following choices: {", ".join(conf.CITIES)}')


def check_args():
    if len(sys.argv) != 3:
        return False
    if not (sys.argv[1].lower() in conf.PROMOTIONS):
        return False
    if not (sys.argv[2].capitalize() in conf.CITIES):
        return False
    return True


if __name__ == '__main__':
    if not check_args():
        display_help()
        exit(0)

    promotion = sys.argv[1].lower()
    city = sys.argv[2].capitalize()

    workbook = xlsxwriter.Workbook(config.FILENAME)
    with open(conf.LOGIN_ID_PAIRS_FILENAME, newline='') as file:
        reader = csv.reader(file, delimiter=';')
        next(reader)
        for row in reader:
            if (row[2] == promotion or promotion == conf.PROMOTIONS[0]) and (row[3] == city or city == conf.CITIES[0]):
                print(f'Processing competencies for {row[1]}')
                extract(workbook, (int(row[0]), row[1]))
    workbook.close()
