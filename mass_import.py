import xlsxwriter

import config
from extract import extract

if __name__ == '__main__':
    workbook = xlsxwriter.Workbook(config.FILENAME)
    for user in config.STUDENT_LOGIN_ID:
        extract(workbook, user)
    workbook.close()
